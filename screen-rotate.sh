#!/bin/bash

# Rotate the screen with touchscreen

declare -A screen_rotation_mapping=( [normal]=0 [left]=1 [inverted]=2 [right]=3 )
declare -a screen_to_touchscreen_rotation_mapping=( 0 2 3 1 )

# This list all input devices that are touch screens, specifically on this laptop.
# Check the results of `xinput list` to identify the devices
declare -a wacom_inputs=(
 `xinput list | awk -F $'\t' '/Wacom Serial Penabled 1FG Touchscreen/ { sub("id=","", $2); print $2 }'`
)


# This may need to be adjusted, if the screen is not designated by LVDS1
screen_rotation_text=$(xrandr -q --verbose | awk '/^LVDS-1/ { print $5; }')
if [[ ! $screen_rotation_text ]]; then
    echo >&2 "Can't get screen rotation"
    exit 1
fi

screen_rotation_num=${screen_rotation_mapping[$screen_rotation_text]}

arg=${1:-"+1"}

if [[ $arg =~ [-+][[:digit:]] ]]; then
    let new_screen_rotation=(screen_rotation_num$arg)\&3
elif [[ $arg =~ [0-3] ]]; then
    new_screen_rotation=$arg
else
    echo >&2 "Argument error"
    exit 1
fi

new_touchscreen_rotation=${screen_to_touchscreen_rotation_mapping[$new_screen_rotation]}

xrandr -o $new_screen_rotation

for dev_id in ${wacom_inputs[@]}; do
    xinput set-prop $dev_id "Wacom Rotation" $new_touchscreen_rotation
done
